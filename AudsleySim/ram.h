#ifndef RAM_H
#define RAM_H
#include <vector>
#include <deque>
#include "task.h"

class Ram
{
public:
    Ram(int,int);

    /**
     * @brief howManyPages This function returns the number of pages that has to be put into the ram for a task
     * @param task the number of the task
     * @param nbPagesNeeded the memory space needed to load the task
     * @return
     */
    int pagesNeeded(int task,int nbPagesNeeded);

    void swapOutFor(int task);
    void swapIn(int task);
    int getNbSwapInTU();
    int getNbSwapOutTU();
    int getRamSize();
    int getLoad();
	int nbPages;
    int freeSpace;
    bool loadTask(std::vector<Task>&,int &, int &);
private:
    int activeSwapInTask;
    int activeSwapOutTask;

    int activeSwapInTU;
    int activeSwapOutTU;
	std::deque<int> pages;
    int load,nbSwapOutTU,nbSwapInTU;
};

#endif // RAM_H
