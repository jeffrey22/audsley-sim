#ifndef SCHEDULER_H
#define SCHEDULER_H
#include <vector>
#include <map>
#include "ram.h"
#include "status.h"

class Scheduler
{
public:
    Scheduler(Ram&,int, int );

    /**
     * @brief Tools::Audsley
     * @param taskSet Le set de taches à planifier. plus l'indice est petit, plus grande est la priorité
     * @return
     */
    bool audsley(std::vector<Task>taskSet);

    /**
     * @brief checkSchedulability check the schedulabilty of the system
     * @param taskSet the set of the tasks that have to be scheduled
     * @return true or false wether the system is schedulable or not
     */
     bool isSchedulable(std::vector<Task>taskSet,bool forceDeadline);

	 int getSystemLoad();
     void printScheduleInfo();
std::vector<Task> finalSet;
private:
    Ram ram;
    int idleTime;
    int interval;
    int preemptions;
    std::vector<std::vector<Status>> taskStatus;
};

#endif // SCHEDULER_H
