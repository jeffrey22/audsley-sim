#include <iostream>
#include <fstream>
#include "tools.h"
#include "ram.h"
#include "scheduler.h"

using namespace std;

int main(int argc, char* argv[])
{
    if (argc < 4) {
        cerr << "Missing arguments." << endl;
        cerr << "Syntax :" << argv[0] << " <taskFile> <ram> <load>" << endl;
    }
    else {
        int totalRam;
        int loadTime = 1;
        ifstream infile;
        string fileName;

        fileName = argv[1];
        infile.open(fileName);
        totalRam = stoi(argv[2]);
        loadTime = stoi(argv[3]);

        if (!infile.is_open()) {
            cerr << "Can't open " << fileName << endl;
        }
        else {

            vector<Task> taskSet = Tools::parseFile(infile); //Parse tasks from file

            if (!taskSet.empty()) {

                double utilization = Tools::getTaskSetUtilization(taskSet);

                if (utilization > 1) {
                    cout << "The system can't be scheduled because its utilization is :" << utilization << endl;
                }
                else {

                    Ram ram(totalRam, loadTime);

                    int period = Tools::lcmOfTaskSet(taskSet);
                    int maxOffset = Tools::maxOffset(taskSet);
                    Scheduler s(ram, period, maxOffset);

                    if (s.audsley(taskSet)) { 
						if (s.isSchedulable(s.finalSet, true))
							s.printScheduleInfo(); //Display schedule info
                    }
                    else {
                        cout << "The system can't be scheduled using Audsley's algorithm." << endl;
                    }
                }
            }
            else {
                cerr << "Provided file is not a valid task file." << endl;
            }
        }
    }

    return 0;
}
