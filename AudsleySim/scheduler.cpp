﻿#include "scheduler.h"
#include "tools.h"
#include <iostream>
#include <map>
#include <cmath>
using namespace std;

Scheduler::Scheduler(Ram& r, int p, int omax)
	: ram(r)
	, interval(omax + 2 * p)
	, preemptions(0)
{
}

bool Scheduler::audsley(std::vector<Task> taskSet)
{
	bool ok = false;
	size_t i = 0;
	while (i < taskSet.size() && !ok) {
		if (isSchedulable(taskSet, false)) { //If the last task is lowest-priority viable
			finalSet.insert(finalSet.begin(), taskSet.back());
			if (taskSet.size() == 1) {
				return true;
			}
			taskSet.pop_back();
			ok = audsley(taskSet); //Continue audsley without the lowest-priority task
		}
		else {
			//Try audsley using another task as lowest-priority task
			taskSet.insert(taskSet.begin(), taskSet.back());
			taskSet.pop_back();
			i++;
		}
	}
	return ok;
}

/**
* @brief Scheduler::isSchedulable
* @param taskSet Set of tasks to be scheduled
* @param forceDeadline If true, all deadlines will be checked, otherwise only lowest priority task's deadline is checked
* @return
*/
bool Scheduler::isSchedulable(std::vector<Task> taskSet, bool forceDeadline)
{
	this->taskStatus = vector<vector<Status> >(taskSet.size(), vector<Status>(this->interval));

	this->ram = Ram(this->ram.getRamSize(), this->ram.getLoad()); //Reinitialize ram

	this->idleTime = this->interval;

	int t = 0;
	while (t < interval) { //Loop through interval to try and schedule tasks

		bool isIdle = true;

		for (size_t taskIndex = 0; taskIndex < taskSet.size(); taskIndex++) {

			if ((forceDeadline || taskIndex == taskSet.size() - 1) &&  //We're either checking all tasks or only the last
				t > taskSet.at(taskIndex).deadline + taskSet.at(taskIndex).offset && //Check if deadline is exceeded
				taskSet.at(taskIndex).wcetLeft != 0) //And the job hasn't finished its work
			{
				return false;
			}

			if (taskSet.at(taskIndex).isReady(t)) { //Ignore task if it's not ready

				bool isBusy = false;

				for (size_t i = 0; i < taskIndex; i++) { //Verify if another task already uses the processor for this timeslot
					if (taskStatus.at(i).at(t) != IDLE) {
						isBusy = true;
						break;
					}
				}
				if (isBusy) { //If the timeslot is used by another task
							  //If this task was busy during the previous timeperiod and it isn't anymore while still having work to do, it is preempted
							  //If wcetleft == wcet, it means the task just finished its work
					if (t > 0 && taskStatus.at(taskIndex).at(t - 1) == BUSY && taskSet.at(taskIndex).wcetLeft != 0 && taskSet.at(taskIndex).wcetLeft != taskSet.at(taskIndex).wcet) {
						preemptions++;
					}
				}
				else //If the processor is not busy, we can use this timeslot for the current task
				{
					isIdle = false;

					int memoryNeeded = ram.pagesNeeded(taskIndex, taskSet.at(taskIndex).memory); //How many more pages need to be loaded in ram for this process?

					if (memoryNeeded > 0) { //If the program still needs more ram

						if (ram.freeSpace >= memoryNeeded) { //If there is enough free space just swap in
							ram.swapIn(taskIndex);
							taskStatus.at(taskIndex).at(t) = SWAP_IN;
						}
						else { //Otherwise swap out another program's page
							ram.swapOutFor(taskIndex);
							taskStatus.at(taskIndex).at(t) = SWAP_OUT;
						}
					}
					else { //If the program doesn't need any more pages it can just run.
						taskStatus.at(taskIndex).at(t) = BUSY;
						taskSet.at(taskIndex).updateWCET();
					}
				}
			}
		}

		if (!isIdle)
			idleTime--;

		t++;
	}

	return true;
}

void Scheduler::printScheduleInfo()
{
	cout << "The following set of tasks is schedulable." << endl;
	cout << "Priority in decreasing order." << endl;
	cout << endl;
	cout << "Offset\t\tPeriod\t\tDeadline\tWCET\t\tRam Pages" << endl;
	for (Task t : finalSet) {
		cout << t.toString("\t\t") << endl;
	}
	cout << endl;
	cout << "Study interval: [0, " << interval << "]" << endl;
	cout << "Number of preemtions: " << preemptions << endl;
	cout << "Total idle time: " << idleTime << endl;
	cout << "System utilization: " << Tools::getTaskSetUtilization(finalSet) * 100 << endl;
	cout << "Time spent on swap ins: " << ram.getNbSwapInTU() << endl;
	cout << "Time spent on swap outs: " << ram.getNbSwapOutTU() << endl;
	cout << "Number of swap ins: " << ram.getNbSwapInTU() / ram.getLoad() << endl;
	cout << "Number of swap outs: " << ram.getNbSwapOutTU() / ram.getLoad() << endl;
	cout << endl;
}

int Scheduler::getSystemLoad()
{
	return 100 * (interval - idleTime) / interval;
}
