#include "ram.h"
#include <deque>
#include <iostream>
using namespace std;

Ram::Ram(int nbPages, int l)
    : freeSpace(nbPages)
    , nbPages(nbPages)
    , load(l)
    , nbSwapOutTU(0)
    , nbSwapInTU(0)
    , activeSwapOutTU(0)
    , activeSwapInTU(0)
{
}

int Ram::pagesNeeded(int task, int nbPagesNeeded)
{
    int pagesLoaded = 0;
    for (int i : pages) {
        if (i == task) {
            pagesLoaded++;
        }
    }
    return nbPagesNeeded - pagesLoaded;
}

void Ram::swapOutFor(int task)
{

    if (activeSwapOutTask == task) //If we started swapping out a page of the task asking a new swapout, we need to restart and swap out another page
        activeSwapOutTU == 0;

    nbSwapOutTU++;
    activeSwapOutTU++;

    if (activeSwapOutTU % load == 0) //If we've spent enough time on the swap out operation we can swap out the page
    {
        for (size_t i = 0; i < pages.size(); i++) {
            if (pages.at(i) == task) { //Move all ram pages of active task to the back of the que
                pages.erase(pages.begin() + i);
                pages.push_back(task);
            }
        }

        pages.pop_front(); //Swap out the oldest page
        freeSpace++;
        activeSwapOutTask = -1;
    }
    else {
        //Store the task whose page is being swapped out (the first page which does not belong to the task asking the swapout)
        for (int page : pages) {
            if (page != task) {
                activeSwapOutTask = page;
                break;
            }
        }
    }
}

void Ram::swapIn(int task)
{

    if (activeSwapInTask != task) //If we've started the swapin of another task's page we need to restart
    {
        activeSwapInTU == 0;
        activeSwapInTask == task;
    }

    nbSwapInTU++;
    activeSwapInTU++;

    if (activeSwapInTU % load == 0) //If we've spent enough time on the swap in operation we can swap in the page
    {
        pages.push_back(task);

        freeSpace--;
        activeSwapInTask = -1;
    }
}

int Ram::getNbSwapInTU()
{
    return nbSwapInTU;
}

int Ram::getNbSwapOutTU()
{
    return nbSwapOutTU;
}

int Ram::getLoad()
{
    return load;
}

int Ram::getRamSize()
{
    return nbPages;
}
