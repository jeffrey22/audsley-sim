TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    tools.cpp \
    task.cpp \
    ram.cpp \
    scheduler.cpp

HEADERS += \
    tools.h \
    task.h \
    ram.h \
    status.h \
    scheduler.h

