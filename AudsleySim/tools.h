#ifndef TOOLS_H
#define TOOLS_H
#include "task.h"
#include <vector>
#include <string>


class Tools
{
public:
	Tools();
	static std::vector<Task> parseFile(std::ifstream&);
	static int lcm(int, int);
	static int lcmOfTaskSet(std::vector<Task>);
	static int maxOffset(std::vector<Task>);
	static double getTaskSetUtilization(std::vector<Task> taskSet);

	static void exportTasksToFile(std::string file, std::vector<Task> taskSet);
};

#endif // TOOLS_H
