#include "task.h"
#include <string>
#include <string.h>
#include <sstream>

using namespace std;

Task::Task()
{
}

Task::Task(const Task& t)
    : offset(t.offset)
    , period(t.period)
    , deadline(t.deadline)
    , wcet(t.wcet)
    , memory(t.memory)
    , wcetLeft(t.wcet)
    , utilization(double(t.wcet) / t.period)
{
}

Task::Task(int offset, int period, int deadline, int wcet, int memory)
{
    this->offset = offset;
    this->period = period;
    this->deadline = deadline;
    this->wcet = wcet;
    this->memory = memory;
    this->utilization = double(wcet) / period;
}

Task::Task(vector<int> infos)
    : offset(infos.at(0))
    , period(infos.at(1))
    , deadline(infos.at(2))
    , wcet(infos.at(3))
    , memory(infos.at(4))
    , wcetLeft(infos.at(3))
    , utilization(double(infos.at(3)) / infos.at(1))
{
}

string Task::toString()
{
    return toString(" ");
}
string Task::toString(string delimiter)
{
    string str = to_string(offset) + delimiter
        + to_string(period) + delimiter
        + to_string(deadline) + delimiter
        + to_string(wcet) + delimiter
        + to_string(memory);

    return str;
}

void Task::updateWCET()
{
    wcetLeft--;

    if (wcetLeft == 0) { //Next job release
        offset += period;
        wcetLeft = wcet;
    }
}

bool Task::isReady(int timeUnit)
{
    return timeUnit >= offset && wcetLeft > 0;
}
