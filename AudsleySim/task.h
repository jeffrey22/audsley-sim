#ifndef TASK_H
#define TASK_H
#include <string>
#include <vector>


class Task
{
public:
    Task();
    Task(const Task&);   
    Task(int, int, int, int, int);
    Task(std::string);
    Task(std::vector<int>);
    /**
     * @brief toString String representation of the task
     * @return a String containing the informationa about the task
     */
    std::string toString();
    std::string toString(std::string delimiter);

    /**
     * @brief synchronize
     */
    void updateWCET();

    /**
     * @brief isReady Tells whether or not the task has to do at the given time
     * @param timeUnit the moment to check on
     * @return true or false;
     */
    bool isReady(int timeUnit);
    int offset;
    int period;
    int deadline;
    int wcet;
    int wcetLeft;
    int memory;
    double utilization;


};

#endif // TASK_H
